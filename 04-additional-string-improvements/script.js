/**
 * String.startsWith()
 * - Returns a boolean;
 * - Case sensitive;
 * - Starting point can be supplied.
 */
const course = 'RFB2';
const flightNumber = '20-AC2018-jz';

console.log(course.startsWith('RFB')); // true
console.log(course.startsWith('rfb')); // false

console.log(flightNumber.startsWith('AC')); // false
console.log(flightNumber.startsWith('AC', 3)); // true


/**
 * String.endsWith()
 * - Returns a boolean;
 * - Case sensitive;
 * - The length of the string to check the end of can be supplied.
 */

const accountNumber = '72837485RT0001';

console.log(accountNumber.endsWith('0001')); // true
console.log(accountNumber.endsWith('RT')); // false
console.log(accountNumber.endsWith('RT', 10)); // true


/**
 * String.includes()
 * - Check if a string is anywhere inside the string to be checked;
 * - Returns a boolean;
 * - Case sensitive.
 */
console.log(flightNumber.includes('AC')); // true
console.log(flightNumber.includes('ac')); // false


/**
 * String.repeat()
 * - Repeat a string;
 * - Returns a string.
 */
const basicString = 'hello';
const repeatedString = basicString.repeat(5);
console.log(repeatedString); // 'hellohellohellohellohello'
