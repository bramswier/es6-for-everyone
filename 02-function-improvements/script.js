/**
 * Arrow functions:
 * - Shorter function syntax;
 * - When supplying only one argument, the parentheses around the argument are optional;
 * - Can be written with an implicit return by leaving out the brackets. To implicitly return an object, wrap the object between parentheses;
 * - Are anonymous; 
 * - Inherits `this` from the lexical scope.
 */
const names = ['john', 'eric', 'mark'];

const fullNames = names.map(function(name) {
	return `${name} johnson`;
})

// Arrow function with one argument and an implicit return
const newFullNames = names.map(name => `${name} johnson`);

// Arrow function with an implicit object return
const fullNamesObject = names.map(name => ({firstName: name, lastName: 'johnson'}));


const button = document.querySelector('[data-button]');

button.addEventListener('click', function() {
	console.log(this); // <button data-button="">Click me</button> (`this` was bound by the function to the element it listens to)
});

button.addEventListener('click', () => {
	console.log(this); // Window (`this` was inherited from the lexical scope, in this case the global scope resulting in the Window object)
});


/**
 * Default parameters:
 * - Can be written to declare what value should be used when an argument is not supplied in a function call;
 * - Are also applied when the value `undefined` is explicitly supplied.
 */
function calculateBill(total, tax = 0.13, tip = 0.15) {
	return total + (total * tax) + (total * tip);
}

const totalBill = calculateBill(100, undefined); // `tax` is supplied as `undefined` and `tip` is left out.
console.log(totalBill); // 128


/**
 * Course exercise - 1
 */

// Select all the list items on the page and convert to array
const listItems = Array.from(document.querySelectorAll('[data-time]'));

// Filter for only the elements that contain the word 'Flexbox'
const flexboxListItems = listItems.filter(listItem => listItem.textContent.includes('Flexbox'));

// map down to a list of time strings
const flexboxTimes = flexboxListItems.map(flexboxListItem => flexboxListItem.dataset.time);

// map to an array of seconds
const flexboxSeconds = flexboxTimes.map(flexboxTime => {
	const splitTime = flexboxTime.split(':');
	const [minutes, seconds] = splitTime;
	const totalSeconds = (parseInt(minutes) * 60) + parseInt(seconds);
	
	return totalSeconds;
});

// reduce to get total
const total = flexboxSeconds.reduce(((previousValue, currentValue) => previousValue + currentValue), 0);
console.log(total); // 6132

// 🔥 This can also be done in a single .reduce(), but we're practicing arrow functions here, so chain them!


/**
 * Course exercise - 2
 */

// 1. Given this array: `[3,62,234,7,23,74,23,76,92]`, use the array filter method and an arrow function to create an array of the numbers greater than `70`
const numbers = [3, 62, 234, 7, 23, 74, 23, 76, 92];

const greaterThanSeventy = numbers.filter(number => number > 70);

console.log(greaterThanSeventy);
