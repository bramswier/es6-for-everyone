/**
 * Proxies allow to override the default behavior of an operation on an object.
 * It receives two arguments: the object to proxy and a 'trap', which is the method to 'hijack' and add behavior to
 */

const person = { name: 'John', age: 100 };
const personProxy = new Proxy(person, {

	// 'Trap' `get`
	// `target` is the proxied object, `property` is the property that was requested
	get(target, property) {
		return target[property].toUpperCase();
	}
});

person.name = 'Jack';
console.log(person.name); // Jack
console.log(personProxy.name); // JACK (the `getter` was 'hijacked' and the return value was modified)


const phoneHandler = {
	// Whenever a property is being set, non-numbers are cleaned
	set(target, name, value) {
		target[name] = value.match(/[0-9]/g).join('');
	},

	// Whener a property is being requested, it's formatted to a phone number format
	get(target, name) {
		return target[name].replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
	}
}

const phoneNumbers = new Proxy({}, phoneHandler);
phoneNumbers.home = '+06-1234-56789';
console.log(phoneNumbers); // Proxy { home: '06123456789' } // `+` and `-` are stripped out
console.log(phoneNumbers.home); // (061) 234-56789 (correctly formatted when being requested)
