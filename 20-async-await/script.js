/**
 * Async - Await
 * - When you execute something synchronously, you wait for the task to be done before you can continue on with the actual code.
 * - When you execute something asynchronously, you start the task, and you immediately move on to the next actual code.
 * 
 * - Async / Await is built on top of Promises. You must have a function that is based on Promises to be able to use Async / Await. An asynchronous function will always return a Promise.
 * 	- A function needs to have the keyword `async` before it.
 *  - `await` needs to be placed before the function call.
 * 
 * - To catch an error, wrap the `await` statement(s) in a try/catch block.
 */

function breathe(amount) {
	return new Promise((resolve, reject) => {
		if (amount < 500) {
			reject('That is too small of a value');
		}
		
		setTimeout(() => resolve(`Breathed for ${amount} milliseconds`), amount);
	});
}

// `async` and `await`
async function go() {
	try {
		const breathing = await breathe(1000).then(response => response);

		//const shortBreathing = await breathe(400); // (in catch) That is too small of a value
		
		return breathing;
	} catch (error) {
		console.error(error);
	}
}

// Kick-off the asynchronous function
go()
	// `.then` can be used since the asynchronous function returns a Promise
	.then(response => console.log(response)); // 'Breathed for 1000 milliseconds'


/**
 * Awaiting multiple Promises.
 */

// async function getData() {
// 	const p1 = fetch('https://api.github.com/users/wesbos').then(response => response.json());
// 	const p2 = fetch('https://api.github.com/users/stolinski').then(response => response.json());
	
// 	const persons = await Promise.all([p1, p2]);
// 	console.log(persons); // [{<json data> }, { <json data> }]
// }

// getData();

async function getData(names) {
	const promises = names.map(name => fetch(`https://api.github.com/users/${name}`)
		.then(response => response.json()));
	const people = await Promise.all(promises);
	console.log(people);
}

getData(['wesbos', 'stolinski', 'darcyclarke']); // [{<json data> }, {<json data> }, { <json data> }]
