/**
 * Destructuring objects: assign a property from an object to a newly declared variable.
 * The order of the variables does not matter since they match the name of the property.
 */

const information = {
	first: 'John',
	last: 'Doe',
	social: {
		twitter: 'twitter.com',
		facebook: 'facebook.com'
	}
}

// Assigning properties the old, repetitive way
// const first = information.first;
// const last = information.last;
// const facebook = information.social.facebook;
// const twitter = information.social.twitter;

// Assigning properties using destructuring
const { first, last } = information;
console.log(first); // 'John'
console.log(last); // 'Doe'

// Assigning and renaming nested properties
const { twitter, facebook: fb } = information.social;
console.log(twitter); // 'twitter.com'
console.log(fb); // 'facebook.com'

// Assigning with variable renaming and default values 
const { w: width = 400, h: height = 500 } = { w: 800 };
console.log(width); // 800 (`w` exists in the destructured object)
console.log(height); // 500 (`h` does not exist in the destructured object)


/**
 * Destructuring arrays: assign an item from an array to a newly declared variable.
 */

// Using destructuring and the rest operator (`...`) to assign items to variables
const team = ['Wes', 'Harry', 'Sarah', 'Keegan', 'Riker'];
const [captain, assistant, ...players] = team;

console.log(captain); // 'Wes'
console.log(assistant); // 'Assistant'
console.log(players); // ['Sarah', 'Keegan', 'Riker']


/**
 * Swapping variables: update existing variables with the value of other variables using array destructuring.
 */

// `let` is used since the variables will be updated
let inRing = 'Hulk Hogan';
let onSide = 'The Rock';

// Create an array of `onSide` and `inRing` and destructure it to `inRing` and `onSide`
[inRing, onSide] = [onSide, inRing];

console.log(inRing); // 'The Rock'
console.log(onSide); // 'Hulk Hogan'


/**
 * Multiple values can be picked and assigned from a returned object using destructuring.
 */

function convertCurrency(amount) {
	const converted = {
		USD: amount * 0.76,
		GPB: amount * 0.53,
		AUD: amount * 1.01,
		MEX: amount * 13.30
	}

	return converted;
}

// Only `converted.AUD` and `converted.USD` are assigned, also in a different order than in the `converted` object
const { AUD, USD } = convertCurrency(100);
console.log(AUD); // 101
console.log(USD); // 76


/**
 * Destructuring objects can be used in function parameters with default parameters. This makes the arguments order independent.
 * If an empty function call is allowed, an empty object needs to be written in the function parameters as a default parameter to match against (so that the empty object will be destructured and the parameters are declared with their default values). 
 */

function calculateTip({ total = 100, tip = 0.15, tax = 0.13 } = {}) {
	return total + (tip * total) + (tax * total);
}

// The order of the arguments do not matter and `tip` is left out
const bill = calculateTip({ tax: 0.20, total: 200 });
console.log(bill); // 270

// No arguments supplied
const defaultBill = calculateTip();
console.log(defaultBill); // 128
