/**
 * Class properties: proposal. Instead of writing every property in the constructor to `this.<property>`, it can be written separately. It still needs to be referenced via `this.<property>`.
 * @link https://github.com/tc39/proposal-class-fields
 */

class Demo {
	constructor() {
		// Current way
		// this.x = 0;
	}

	// Class property proposal: written separately
	//x = 0;

	logX() {
		this.x = this.x + 1;
		console.log(this.x);
	}
}

const foo = new Demo();

/**
 * String methods: `padStart` and `padEnd`.
 */

console.log('John'.padStart(6, '.')); // ..John
console.log('John'.padEnd(8, '__')); // John____

/**
 * Function arguments trailing commas.
 */

function family(mom, dad,) {
	
}

/**
 * Object.entries() and Object.values()
 */

const inventory = {
	backpacks: 10,
	jeans: 23,
	hoodies: 4,
	shoes: 11
}

// array of keys
console.log(Object.keys(inventory)); // ["backpacks", "jeans", "hoodies", "shoes"]

// array of values
console.log(Object.values(inventory)); // [10, 23, 4, 11]

// array of arrays with [0]: the key and [1] the value
console.log(Object.entries(inventory)); // [Array(2), Array(2), Array(2), Array(2)]]

// Make a nav for the inventory
const nav = Object.keys(inventory).map(item => `<li>${item}</li>`).join('');
console.log(nav);

// tell us how many values we have
const totalInventory = Object.values(inventory).reduce((a, b) => a + b);
console.log(totalInventory);

// Print an inventory list with numbers
Object.entries(inventory).forEach(([key, val]) => {
	console.log(key, val);
});

for (const [key, val] of Object.entries(inventory)) {
	console.log(key);
	if (key === 'jeans') break;
}
