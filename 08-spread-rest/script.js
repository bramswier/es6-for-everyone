/**
 * Spread operator (`...`) in a new array: apply every item from an Iterable (the operand) to the new array.
 */

const featured = ['Deep Dish', 'Peperoni', 'Hawaiian'];
const specialty = ['Meatzza', 'Spicy Mama', 'Margherita'];

// Combining two arrays the old way
const pizzas = featured.concat(specialty);

// Combining two arrays using the spread operator and adding a new item in between
const pizzasCombined = [...featured, 'vegetarian', ...specialty];


// Assigning an existing array to a variable does not create a new array, but a reference
// Changes in the variable are applied to the referenced array as well
const fridayPizzas = featured;
fridayPizzas[0] = 'Cheese';
console.log(fridayPizzas); // ["Cheese", "Peperoni", "Hawaiian"]
console.log(featured); // ["Cheese", "Peperoni", "Hawaiian"] (referenced array is altered)

// When using the spread operator, changes in the variable are not applied to the spreaded array
const saturdayPizzas = [...specialty];
saturdayPizzas[0] = 'Cheese';
console.log(saturdayPizzas); // ["Cheese", "Spicy Mama", "Margherita"]
console.log(specialty); // ["Meatzza", "Spicy Mama", "Margherita"] (spreaded array is unaltered)


/**
 * Course exercise - wrap a <span> around every character in a title.
 */

const titleElement = document.querySelector('[data-title]');
const titleText = titleElement.textContent; // string
const characters = [...titleText]; // a string is an Iterable, so the spread operator can be used to retrieve each character
const characterTags = characters.map(character => `<span>${character}</span>`).join('');

titleElement.innerHTML = characterTags;


/**
 * Spread operator in a function call: apply every item in an Iterable as separate argument.
 */

const inventors = ['Einstein', 'Newton', 'Galileo'];
const newInventors = ['Musk', 'Jobs'];

// Without the spread operator, the whole `newInventors` array would be the argument, resulting in `inventors` containing an array as its last item
inventors.push(...newInventors);
console.log(inventors); // ['Einstein', 'Newton', 'Galileo', 'Musk', 'Jobs']

/**
 * Rest parameter (`...`): captures and returns the 'rest' of received arguments into an array
 */

function convertCurrency(rate, ...amounts) {
	console.log(rate); // 1.15
	console.log(amounts); // [10, 20, 50]
	return amounts.map(amount => rate * amount);
}

const converted = convertCurrency(1.15, 10, 20, 50);
console.log(converted); // [11.5, 23, 57.49999999999999]


// Using the rest operator with destructuring
const runner = ['John Doe', 43859, 5.5, 5, 3, 6, 35];
const [name, id, ...runs] = runner;
console.log(name, id, runs); // John Doe, 43859, [5.5, 5, 3, 6, 35]
