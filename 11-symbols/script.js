/**
 * `Symbol`: a primitive type. A unique identifier used to prevent naming collisions. 
 * It receives a description argument, but this will not be its 'real' name (which is a unique identifier).
 * Can not be looped. To retrieve the a Symbol property name from an object, use `Object.getOwnPropertySymbols()`.
 * To retrieve its data, map over it.
 */

const john = Symbol('John');
const person = Symbol('John');

console.log(john); // Symbol(John)
console.log(person); // Symbol(John)

console.log(john === person); // false
console.log(john == person); // false


// An object with a naming collision in property names
const classRoom = {
	'mark': { grade: 50, gender: 'male' },
	'olivia': { grade: 50, gender: 'female' },
	'olivia': { grade: 50, gender: 'female' }
}

// Using `Symbol` and computed property names to avoid naming collisions in property names
const newClassRoom = {
	[Symbol('mark')]: { grade: 50, gender: 'male' },
	[Symbol('olivia')]: { grade: 50, gender: 'female' },
	[Symbol('olivia')]: { grade: 50, gender: 'female' }
}

const symbols = Object.getOwnPropertySymbols(newClassRoom);
console.log(symbols); // [Symbol(mark), Symbol(olivia), Symbol(olivia)]

const data = symbols.map(symbol => newClassRoom[symbol]);
console.log(data); // {grade: 50, gender: 'male'}, {grade: 50, gender: 'female'}, {grade: 50, gender: 'female'}
