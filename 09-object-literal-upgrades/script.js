/**
 * Object literal improvements:
 * - When assigning a variable to a property which share the same name, the double colon (`:`) and variable name can be left out;
 * - When creating a method, the double colon (`:`) and function tag (`function`) can be left out;
 * - Computed properties allow an expression in the property name using `[]`.
 */

const first = 'Snickers';
const last = 'Bos';
const age = 2;
const breed = 'King Charles Cav';

const dog = {
	// Assigning the old way
	first: first,
	last: last,

	// Assigning the new way (only when the variable name is the same as the property name)
	age,
	breed,

	// Creating a method the old way
	bark: function() {
		console.log('woof');
	},

	// Creating a method the new way
	sit() {
		console.log('sits');
	},

	// Using a computed property, resulting in property 'weight'
	['wei' + 'ght']: 40
};

console.log(dog); // { first: 'Snickers', last: 'Bos', age: 2, breed: 'King Charles Cav', bark: ƒ (), sit: ƒ sit(), weight: 40 }
