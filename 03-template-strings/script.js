/**
 * Template literals: strings containing an expression. 
 * Written in backticks (``). Expressions are within a `${}` block. 
 * Can be written multiline without a `\` on each line and tabs/spaces are maintained in the output.
 */
const name = 'Snickers';
const age = 2;

// Strings containing an expression written the old way
const oldSentence = 'My dog ' + name + ' is ' + age * 7 + ' years old';

// Template literal
const newSentence = `My dog ${name} is ${age * 7} years old.`;
console.log(newSentence); // My dog Snickers is 14 years old.


// Multiline written the old way
const oldMultiline = 'Hello there, \
	how are you? \
';

const dogs = [
	{ name: 'Snickers', age: 2 },
	{ name: 'Hugo', age: 8 },
	{ name: 'Sunny', age: 1 }
];

// Multiline using template strings (with a template literal inside)
const markup = `
	<ul class="dogs">
		${dogs.map(dog => `<li> ${dog.name} is ${dog.age} </li>`).join('')}
	</ul>
`;

document.body.innerHTML = markup;


function randomNumber(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

// A function call inside a template literal
const message = `The random number is ${randomNumber(1, 100)}`;


/**
 * Tagged template literals: parse template literals using a function.
 * The first argument contains an array of strings, the remaining arguments are expressions.
 * The array containing the strings is always 1 item larger than the received expressions.
 */

// A rest operator is used to group all received expressions
function highlight(strings, ...values) {
	let markedUpString = '';

	// Create a couple of string item and expression item
	strings.forEach((string, index) => {
		markedUpString += `${string} <span style="background: papayawhip;">${values[index] || ''}</span>`;
	});

	return markedUpString;
};

const dogName = 'Snickers';
const dogAge = 5;
const sentence = highlight`My dog's name is ${name} and he is ${age} years old.`;
document.body.innerHTML = document.body.innerHTML + sentence;
