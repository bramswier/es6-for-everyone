/**
 * `Array.from()` converts and returns an Array-like object to an array. Accepts an optional map function.
 */

// `querySelectorAll` returns a NodeList (Array-like), so it needs to be converted to a true array to use `.map()`
const people = Array.from(document.querySelectorAll('[data-person]'));
const names = people.map(person => person.textContent);
console.log(names); // ['John', 'George', 'Tom']


// A map function is supplied as second argument
const fullNames = Array.from(document.querySelectorAll('[data-person]'), person => `${person.textContent} Doe`);
console.log(fullNames); // ['John Doe', 'George Doe', 'Tom Doe']

/**
 * `Array.of()` creates and returns an array of its received arguments.
 */

const ages = Array.of(10, 40, 23, 58, 18, 54);
console.log(ages); // [10, 40, 23, 58, 18, 54]


/**
 * `Array.find()` runs a test on each item in the array (via a supplied function) and returns the value of the first item that passes the test, otherwise `undefined` is returned.
 */

const posts = [
	{ id: 3879503, content: 'flower' },
	{ id: 0431245, content: 'beer' },
	{ id: 8428937, content: 'chair' }
];

const data = posts.find(post => post.id === 0431245);
console.log(data); // { id: 144037, content: 'beer' }


/**
 * `Array.findIndex()` runs a test on each item in the array (via a supplied function) and returns the index of the first item that passes the test, otherwise `-1` is returned. 
 */

const postIndex = posts.findIndex(post => post.id === 8428937);
console.log(postIndex); // 2
