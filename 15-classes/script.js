class Dog {
	constructor(name, breed) {
		this.name = name;
		this.breed = breed;
	}

	bark() {
		console.log(`Bark, my name is ${this.name}`);
	}

	cuddle() {
		console.log(`Cuddles owner`);
	}

	// Static methods only live on the class object, not on the instance
	static info() {
		console.log('I am a dog');
	}

	// Setter: this function will be called when the property `nickname` is changed
	set nickname(value) {
		this.nick = value;
	}

	// Getter: this function will be called when the property `nickname` is looked up
	get nickname() {
		return this.nick;
	}
}

const snickers = new Dog('Snickers', 'King Charles');
Dog.info(); // 'I am a dog'
snickers.nickname = 'Snicky';
console.log(snickers.nickname); // 'Snicky'


/**
 * Extending a class.
 */

class Animal {
	constructor(name) {
		this.name = name;
		this.thirst = 100;
		this.belly = []
	}

	drink() {
		this.thirst -= 10;
		return this.thirst;
	}

	eat(food) {
		this.belly.push(food);
		return this.belly;
	}
}

const rhino = new Animal('Rhiney');
console.log(rhino.eat('leaves')); // ['leaves']

class Cat extends Animal {
	constructor(name, breed) {
		// `super()`: call the class that you're extending (required), with the arguments it needs
		super(name)
		this.breed = breed;
	}

	meow() {
		console.log(`Meow I'm a cat`);
	}
}

const whiskers = new Cat('Whiskers', 'Siamese');
console.log(whiskers.eat('mouse')); // ['mouse'];
console.log(whiskers.meow()); // 'Meow I'm a cat'


