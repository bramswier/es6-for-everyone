/**
 * Usually, a function runs from top to bottom when called. A generator function can be 'started and stopped' for an indefinite amount of time, and it lets you pass additional information to it at a later point in time.
 * Consists of `*` and `yield`.
 * 	- `*` is written next to the `function` keyword
 * 	-`yield` returns one item untill the function is called again
 * To get the item yielded, use `.next()`. This returns an object containing the yielded value, and a property (`done`) indicating whether the generator is done.
 */

function* listPeople() {
	yield 'John';
	yield 'Jack';
	yield 'Thomas';
}

const people = listPeople();
console.log(people); // listPeople { <suspended> } (The generator is assigned)

const firstPerson = people.next();
console.log(firstPerson); // { value: 'John', done: false }

const secondPerson = people.next();
console.log(secondPerson); // { value: 'Jack', done: false }

const thirdPerson = people.next();
console.log(thirdPerson); // { value: 'Thomas', done: false }

const unknown = people.next();
console.log(unknown); // { value: undefined, done: true }


/**
 * Creating a generator 
 */
const inventors = [
	{ first: 'Albert', last: 'Einstein', year: 1879 },
	{ first: 'Isaac', last: 'Newton', year: 1643 },
	{ first: 'Galileo', last: 'Galilei', year: 1564 },
	{ first: 'Marie', last: 'Curie', year: 1867 },
	{ first: 'Johannes', last: 'Kepler', year: 1571 },
	{ first: 'Nicolaus', last: 'Copernicus', year: 1473 },
	{ first: 'Max', last: 'Planck', year: 1858 },
];

function* generateLoop(array) {
	// This will create a `yield` for each inventor
	for (const item of array) {
		yield item;
	}
}

const generatedInventors = generateLoop(inventors);

console.log(generatedInventors.next()); // { value: { first: "Albert", last: "Einstein", year: 1879 }, done: false }
console.log(generatedInventors.next()); // { value: { first: "Isaac", last: "Newton", year: 1643 }, done: false }
// ...


/**
 * Using Generators with Ajax
 */
function ajax(url) {
	// In the last `.then()`, the result is assigned to the `yield` that called `ajax()`
	fetch(url).then(data => data.json()).then(data => dataGen.next(data))
}

function* steps() {
	console.log('fetching beers');
	const beers = yield ajax('http://api.react.beer/v2/search?q=hops&type=beer');
	console.log(beers);

	console.log('fetching wes');
	const wes = yield ajax('https://api.github.com/users/wesbos');
	console.log(wes);

	console.log('fetching fat joe');
	const fatJoe = yield ajax('https://api.discogs.com/artists/51988');
	console.log(fatJoe);
}

const dataGen = steps();
dataGen.next(); // kick it off


/**
 * Looping generators with `for...of`
 */
function* lyrics() {
	yield `But don't tell my heart`;
	yield `My achy breaky heart`;
	yield `I just don't think he'd understand`;
	yield `And if you tell my heart`;
	yield `My achy breaky heart`;
	yield `He might blow up and kill this man`;
}

// Assigning the Generator (which is an Iterable)
const achy = lyrics();

// Each `yield` will be logged
for (const line of achy) {
	console.log(line);
}

