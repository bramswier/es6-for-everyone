/**
 * Promises:
 * - represent an eventual completion or failure of an asynchronous operation and its resulting value;
 * - returns a Promise object to which callback functions can be attached (allowing chaining);
 * - `.then()` is called when a promise is resolved, and returns a new Promise;
 * - `.catch()` is called when a promise is rejected (failed), and returns a new Promise;
 * 	- `.catch()` is actually a `.then()` in which the second argument is the error callback function. 
 * 		This means that a `.then()` after a `.catch()` will always be called (since the 'hidden' `.then()` in `.catch()` as described above is always called, but returns `undefined` when there was no error)
 * - `.finally()` is always called last, whether the promise was resolved or rejected;
 */

// Using promise chaining on `.fetch()`
const postsPromise = fetch('https://wesbos.com/wp-json/wp/v2/posts');

postsPromise
	.then(response => response.json()) // `response` is the HTTP response, `.json()` extracts the JSON body content 
	.then(json => console.log(json)) // <data>
	.catch(error => console.log(error))
	.then(empty => console.log(empty)) // undefined (still called)
	.finally(() => console.log('done, no arguments received')); // 'done, no arguments received' (always called)

/**
 * Creating a Promise:
 * - `resolve` is called when the Promise resolved;
 * - `reject` is called when the Promise rejected (failed).
 */

const promise = new Promise((resolve, reject) => {
	setTimeout(() => {
		resolve('data');

		// reject(new Error('Data can not be retrieved'));
	}, 1000);
});
 
promise
	.then(data => console.log(data)) // 'data'
	.catch(error => console.error(error)); // Error: Data can not be retrieved (uncomment `reject()`)
 

/**
 * `Promise.all()` resolves when every Promise supplied is resolved (if any Promise rejects, `.all()` rejects and `.catch()` will be called with the error from the first rejecting Promise)
 */

const firstPromise = new Promise((resolve, reject) => {
	setTimeout(() => {
		resolve('firstPromise resolved');
	}, 1000);
})

const secondPromise = new Promise((resolve, reject) => {
	setTimeout(() => {
		reject(new Error('secondPromise rejected'));
	}, 2000);
})

Promise
	.all([firstPromise, secondPromise])
	.then(data => console.log(data))
	.catch(error => console.error(error)); // Error: secondPromise rejected
