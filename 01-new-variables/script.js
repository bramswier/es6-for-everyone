/**
 * `var` can be reassigned and redeclared.
 */
var width = 100;
console.log(width); // 100


width = 200;
console.log(width); // 200


var width = 300;
console.log(width); // 300


/**
 * `var` is only function scoped. 
 * `let` and `const` are block scoped (anything between a `{}`). 
 */
function setHeight() {
	var height = 100;
}
console.log(height); // Uncaught ReferenceError: height is not defined (function scoped)


if (10 > 5) {
	var years = 100;
	let age = 50;
	const password = 'secret';
}
console.log(years); // 100 (`years` is 'leaked' to the global scope)
console.log(age); // Uncaught ReferenceError: age is not defined (block scoped)
console.log(password); // Uncaught ReferenceError: password is not defined (block scoped)


/**
 * `let` and `const` can not be redeclared.
 * `let` can be reassigned.
 * `const` can not be reassigned. However, when it is an object, its attributes can be reassigned.
 */
let winner = true;
let winner = false; // Uncaught SyntaxError: Identifier 'winner' has already been declared


winner = false;
console.log(winner); // false


if (10 > 5) {
	let winner = true;
}
console.log(winner); // false (`winner` inside the if-statement (block scope) is a different `winner` declared outside the if-statement)


const key = 'secretkey';
key = 'updatedkey'; // Uncaught TypeError: Assignment to constant variable.


const person = {
	name: 'Jack',
	age: 30
}
person.age = 31;
console.log(person); // { name: 'Jack', age: 31 }


/**
 * Due to block scoping of `let` and `const`, an IIFE is no longer necessary as a way to prevent variables (`var`) being set in the global scope.
 * `let` and `const` can simply be declared in `{}` to prevent it from being set in the global scope.
 */
var time = 60;
console.log(time); // 60 (`time` is in the global scope)

(function() {
	var temperature = '25';
})();
console.log(temperature); // Uncaught ReferenceError: temperature is not defined

{
	const color = 'green';
}
console.log(green); // Uncaught ReferenceError: green is not defined


/**
 * Using `let` in a for-loop keeps the `let` in a for-loop.
 * Also, this `let` is a different `let` each iteration due to block scoping (see line 49 - 52).
 */
for (var i = 0; i < 2; i++) {
	setTimeout(function() {
		console.log('i is ' + i); // 'i is 2' (`i` is overwritten every iteration)
	}, 1000);
}
console.log(i); // 2 (`i` is set in the global scope)


for (let j = 0; j < 2; j++) {
	setTimeout(function() {
		console.log('j is ' + j); // 'j is 0', 'j is 1' (`let j` is a different `let` every iteration due to block scoping)
	}, 1000);
}
console.log(j); // Uncaught ReferenceError: j is not defined


/**
 * `let` and `const` are in the 'temporal dead zone', which means that their value can not be accessed before they are declared.
 */
console.log(pizza); // undefined (`var pizza` exists, but holds no value)
var pizza = 'margherita';


console.log(tea); // Uncaught ReferenceError: tea is not defined
const tea = 'green';
