/**
 * `Set`: a list of items to add to, remove from, loop over and get information of.
 * It only contains values and is not index-based. Items can be requested without having to know its index.
 */

const people = new Set(['John', 'Jack', 'Thomas']);
console.log(people.has('Jack')); // true

people.add('John'); // ('John' already exists)
console.log(people); // Set(3) { 'John', 'Jack', 'Thomas' } (No double entry)

console.log(people.size); // 3

people.delete('John');
console.log(people); // Set(2) { 'Jack', 'Thomas' }

people.clear();
console.log(people); // Set(0) { }

people.add('George');
people.add('Frank');
console.log(people) // Set(2) { 'George', 'Frank' }

for (const person of people) {
	console.log(person); // 'George', 'Frank'
}

// Creating an iterator
const peopleIterator = people.values();
console.log(peopleIterator.next()); // { value: 'George', done: false }
console.log(peopleIterator.next()); // { value: 'Frank', done: false }

console.log(people[0]); // undefined (since it's not index based)


// When `next` is called, the returned item is removed from the Iterator
people.add('Sarah');
people.add('Simone');

const lineOfPeople = people.values();

lineOfPeople.next();

console.log(people); // Set(4) { 'George', 'Frank', 'Sarah', 'Simone' }
console.log(lineOfPeople); // SetIterator { 'Frank', 'Sarah', 'Simone' } ('George' is removed)


/**
 * WeakSet: like `Set`, but differs:
 * 	- Can only contain objects;
 *  - Can not be looped over;
 *  - No `clear` method;
 *  - WeakSets clean themself up: when an object that a WeakSet 'contains' (references to) is deleted, it's also deleted from the WeakSet.
 */

let dog1 = { name: 'Snickers', age: 3 };
let dog2 = { name: 'Sunny', age: 1 };

const dogs = new WeakSet([dog1, dog2]);

console.log(dogs); // WeakSet { {…}, {…} }

dog1 = null;

console.log(dogs); // `dog` should be removed from the WeakSet (due to a memory leak it's still here when you view it directly in the browser, but if you wait a few seconds in the browser and log it again, it's removed)
