import { uniq } from 'lodash';
import insane from 'insane';
import jsonp from 'jsonp';

// When exported as a default, you can give the import a custom name
// import customNameApiKey from './src/config';
// console.log(customNameApiKey);

// Importing named exports (via destructuring, since the import will come in as an object), and renaming
import { apiKey as key, url, sayHi, old, dog } from './src/config';

// Importing default and named
// import User, { creatURL, gravatar } from './src/user';

console.log(key);
console.log(url);
sayHi('John');
console.log(old);
console.log(dog);

const ages = [1, 1, 4, 52, 12, 4];

console.log(uniq(ages));
