// Variables in module are scoped to module (not leaking to global window)

// Named exports
export const apiKey = 'abc123';

export const url = 'https://google.com';

export function sayHi(name) {
	console.log(`hello ${name}`);
}

const age = 100;
const dog = 'snickers';

// Renaming named exports (and object literal improvement: variable name is left out)
export { age as old, dog };
 
// Default export: when importing, the whole module exported module will be imported
// Named export: when importing, the importer can hand-pick the things to import (methods, variables)

// Default export
// const apiKey = 'abc123';
// export default apiKey;
