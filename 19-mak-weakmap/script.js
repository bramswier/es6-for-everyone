/**
 * `Map`: like `Set`, but object-based in key-value pairs. Any value can be used as a key or value.
 */
const dogs = new Map();

dogs.set('Snickers', 3);
dogs.set('Sunny', 2);
dogs.set('Hugo', 10);

console.log(dogs.has('Sunny')); // true
console.log(dogs.get('Hugo')); // 10
// ... other methods

dogs.forEach((val, key) => console.log(val, key)); // 3 'Snickers', 2 'Sunny', 10 'Hugo'

// By default in a `for...of` loop, each item is returned in an array with [0]: the value and [1]: the key
for (const dog of dogs) {
	console.log(dog); // ['Snickers', 3], ['Sunny', 2], ['Hugo', 10]
}

// Using destructuring
for (const [key, val] of dogs) {
	console.log(key, val); // Snickers 3, Sunny 2, Hugo 10
}


/**
 * WeakMap: like `WeakSet`, doesn't have a size, can not be looped over, and items will be removed when they do not exist outside the WeakMap.
 */
let dog1 = { name: 'Snickers' };
let dog2 = { name: 'Sunny' };

const strong = new Map();
const weak = new WeakMap();

strong.set(dog1, 'Snickers is the best!');
weak.set(dog2, 'Sunny is the 2nd best!');

dog1 = null;
dog2 = null;

console.log(strong); // should still contain the object it was set
console.log(weak); // should be empty (see WeakSet example)
