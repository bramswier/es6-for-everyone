/**
 * `for...of`: loop over 'iterable objects'. More versatile than existing Array loops.
 */

const cuts = ['Chuck', 'Brisket', 'Shank', 'Short Rib'];

// `for` downside: cluttered syntax, no singular name of the looped variable
for (let i = 0; i < cuts.length; i++) {
	console.log(cuts[i]);
}

// `forEach` downside: can not use `break` to abort the loop or `continue` to skip an item
cuts.forEach(cut => console.log(cut));

// `for...in` downside: returns index, loops over properties and methods manually added to the array and the Array prototype
Array.prototype.shuffle = function(){};
cuts.shop = 'Meats';
for (const index in cuts) {
	console.log(cuts[index]); // 'Chuck', 'Brisket', 'Shank', 'Short Rib', 'Meats', ƒ (){}
}

// `for...of`: item name is returned, manually added properties/methods are ignored, `break` and `contintue` can be used
for (const cut of cuts) {
	console.log(cut); // 'Chuck', 'Brisket'

	if (cut === 'Brisket') {
		break;
	}
}

/**
 * Using `for...of` on Iterables
 */

// cuts.entries() (`Object.entries()`) returns an array (`Array Iterator`) of its own enumerable properties in a key-value pair, which means it can be destructured
for (const [index, cut] of cuts.entries()) {
	console.log(`${cut} is index ${index}`); // Chuck is index 0, Brisket is index 1, ...
};

// `arguments` is not an array but an Array-like object (its prototype is Object.prototype, not Array.prototype) so array loops do not work, but `for...of` does work since it's an Iterable
function addUpArguments() {
	let total = 0;

	for (const argument of arguments) {
		total += argument;
	}

	console.log(total);
}

addUpArguments(1, 2, 34); // 37

// A string is also an iterable
const name = 'John Doe';
for (const character of name) {
	console.log(character); // 'J', 'o', 'h', 'n', ' ', 'D', 'o', 'e'
}

// `querySelectorAll` returns an Array-like object (so it's an Iterable). Instead of transforming it to an Array (such as using `Array.from()`) before looping over it, `for...of` can be used directly on it
const paragraphs = document.querySelectorAll('[data-paragraph]');
for (const paragraph of paragraphs) {
	console.log(paragraph); // <p data-paragraph="">text</p>, <p data-paragraph="">text</p>, ...
}


// Objects can not be iterated with `for...of` by default, however when supplying the object to Object.entries(), an iterable is returned so that it can be iterated (`Object.keys()` can also be used, but the indexes are not returned))
const apple = {
	color: 'red',
	size: 'medium',
	weight: 50,
	sugar: 10
}

for (const [index, property] of Object.entries(apple)) {
	console.log(`${index} is ${property}`); // color is 'red', size is 'medium', weight is 50, sugar is 10
}
